<?php

require_once('shaun.php');
require_once('animal.php');

$sheep = new Animal("shaun");
echo "Name : ".$sheep->name."<br>"; 
echo "Legs : ".$sheep->legs."<br>";
echo "Cold blooded : ".$sheep->cold_blooded."<br><br>"; 

$kodok = new Frog("buduk");
echo "Name : ".$kodok->name. "<br>"; // "buduk"
echo "Legs : ".$kodok->legs. "<br>"; // 4
echo "Cold Blooded : ".$kodok->cold_blooded."<br>"; 
echo "Jump : ".$kodok->jump."<br><br>" ; 

$sungokong = new Ape("kera sakti");
echo "Name : ".$sungokong->name."<br>"; 
echo "Legs : ".$sungokong->legs."<br>"; // 4
echo "Cold Blooded : ".$sungokong->cold_blooded."<br>";
echo "Yell : ".$sungokong->yell."<br>";
?>

