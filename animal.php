<?php
     class frog {
        public $name;
        public $legs = 4;
        public $cold_blooded = "no.";
        public $jump = "Hop Hop";

        public function __construct($string){
            $this->name = $string;
        }
    }

    class Ape {
        public $name;
        public $legs = 2;
        public $cold_blooded = "no.";
        public $yell = "Auooo";

        public function __construct($string){
            $this->name = $string;
        }
    }
?>